
//---> Header
tt_content.gridelements_pi1.20.5 = TEXT
tt_content.gridelements_pi1.20.5 {
    required = 1
    stdWrap.fieldRequired = header
    stdWrap.if {
        equals.field = header_layout
        value = 100
        negate = 1
    }
    field = header
    dataWrap = <h{field:header_layout}> | </h{field:header_layout}>
}
//---> GidElements
tt_content.gridelements_pi1.20.10.setup {
    containers-1 < lib.gridelements.defaultGridSetup
    containers-1 {
        cObject = FLUIDTEMPLATE
        cObject {
            file = EXT:mf_foundation/Resources/Private/Templates/GridElements/Grid.html
            layoutRootPath = {$page.pathToPrivate}Fluid/Layouts/Content/
            partialRootPath = {$page.pathToPrivate}Fluid/Partials/Content/
        }
    }
    containers-2 < .containers-1
    containers-3 < .containers-1
    containers-6 < .containers-1
    containers-9 < .containers-1
}