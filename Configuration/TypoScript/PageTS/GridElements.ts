
tx_gridelements {
    # TS Elemente haben Vorrang vor Datensätzen mit gleichen IDs
    overruleRecords = 0
    setup {
        # Bezeichung des Elements (muss nicht zwingend eine ID sein)
        containers-1 {
            title = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-1.title
            description = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-1.description
            topLevelLayout = 0
            icon = EXT:mf_foundation/Resources/Public/Images/containers-1.jpg
            # Konfiguration die normal im Datensatz stehen würde
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = 1. Container
                                colPos = 101
                                allowed = *
                            }
                        }
                    }
                }
            }
        }
        containers-2 < .containers-1
        containers-2 {
            title = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-2.title
            description = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-2.description
            topLevelLayout = 0
            icon = EXT:mf_foundation/Resources/Public/Images/containers-2.jpg
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            2 {
                                name = 2. Container
                                colPos = 102
                                allowed = *
                            }
                        }
                    }
                }
            }
        }
        containers-3 < .containers-2
        containers-3 {
            title = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-3.title
            description = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-3.description
            topLevelLayout = 0
            icon = EXT:mf_foundation/Resources/Public/Images/containers-3.jpg
            config {
                colCount = 3
                rowCount = 1
                rows {
                    1 {
                        columns {
                            3 {
                                name = 3. Container
                                colPos = 103
                                allowed = *
                            }
                        }
                    }
                }
            }
        }
        containers-6 < .containers-3
        containers-6 {
            title = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-6.title
            description = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-6.description
            topLevelLayout = 0
            icon = EXT:mf_foundation/Resources/Public/Images/containers-6.jpg
            config {
                colCount = 3
                rowCount = 2
                rows {
                    2 {
                        columns {
                            1 {
                                name = 4. Container
                                colPos = 104
                                allowed = *
                            }
                            2 {
                                name = 5. Container
                                colPos = 105
                                allowed = *
                            }
                            3 {
                                name = 6. Container
                                colPos = 106
                                allowed = *
                            }
                        }
                    }
                }
            }
        }
        containers-9 < .containers-6
        containers-9 {
            title = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-9.title
            description = LLL:EXT:mf_foundation/Resources/Private/Language/locallang_be.xlf:containers-9.description
            topLevelLayout = 0
            icon = EXT:mf_foundation/Resources/Public/Images/containers-9.jpg
            config {
                colCount = 3
                rowCount = 3
                rows {
                    3 {
                        columns {
                            1 {
                                name = 7. Container
                                colPos = 107
                                allowed = *
                            }
                            2 {
                                name = 8. Container
                                colPos = 108
                                allowed = *
                            }
                            3 {
                                name = 9. Container
                                colPos = 109
                                allowed = *
                            }
                        }
                    }
                }
            }
        }
    }
}