<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$_EXTKEY = 'mf_foundation';
$ll = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xlf:';
$llBe = 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_be.xlf:';

$foundationColumns = [
    'tx_mffoundation_columnss' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_columnss',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_columnss.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.1', 'small-12'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.2', 'small-9'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.3', 'small-8'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.4', 'small-6'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.5', 'small-4'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.6', 'small-3'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.7', 'small-1'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.8', 'small-2'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.9', 'small-5'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.10', 'small-7'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.11', 'small-10'],
                [$ll . 'tt_content.tx_mffoundation_columnss.I.12', 'small-11'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_columnsm' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_columnsm',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.1', 'medium-12'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.2', 'medium-9'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.3', 'medium-8'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.4', 'medium-6'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.5', 'medium-4'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.6', 'medium-3'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.7', 'medium-1'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.8', 'medium-2'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.9', 'medium-5'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.10', 'medium-7'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.11', 'medium-10'],
                [$ll . 'tt_content.tx_mffoundation_columnsm.I.12', 'medium-11'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_columnsl' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_columnsl',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.1', 'large-12'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.2', 'large-9'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.3', 'large-8'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.4', 'large-6'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.5', 'large-4'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.6', 'large-3'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.7', 'large-1'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.8', 'large-2'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.9', 'large-5'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.10', 'large-7'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.11', 'large-10'],
                [$ll . 'tt_content.tx_mffoundation_columnsl.I.12', 'large-11'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_showbyscreensize' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_showbyscreensize',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.1', 'show-for-small-only'],
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.2', 'show-for-medium-up'],
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.3', 'show-for-medium-only'],
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.4', 'show-for-large-up'],
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.5', 'show-for-large-only'],
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.6', 'show-for-xlarge-up'],
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.7', 'show-for-xlarge-only'],
                [$ll . 'tt_content.tx_mffoundation_showbyscreensize.I.8', 'show-for-xxlarge-up'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_hidebyscreensize' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_hidebyscreensize',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.1', 'hide-for-small-only'],
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.2', 'hide-for-medium-up'],
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.3', 'hide-for-medium-only'],
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.4', 'hide-for-large-up'],
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.5', 'hide-for-large-only'],
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.6', 'hide-for-xlarge-up'],
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.7', 'hide-for-xlarge-only'],
                [$ll . 'tt_content.tx_mffoundation_hidebyscreensize.I.8', 'hide-for-xxlarge-up'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_incompleterow' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_incompleterow',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_incompleterow.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_incompleterow.I.1', 'end'],
            ],
            'size' => 1,
            'maxitems' => 1,c
        ],
    ],
    'tx_mffoundation_fluidrow' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_fluidrow',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_fluidrow.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_fluidrow.I.1', 'expanded'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_sameheight' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_sameheight',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_sameheight.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_sameheight.I.1', 'data-equalizer'],
                [$ll . 'tt_content.tx_mffoundation_sameheight.I.2', 'data-equalizer-watch'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_offsets' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_offsets',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_offsets.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.1', 'small-offset-1'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.2', 'small-offset-2'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.3', 'small-offset-3'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.4', 'small-offset-4'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.5', 'small-offset-5'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.6', 'small-offset-6'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.7', 'small-offset-7'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.8', 'small-offset-8'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.9', 'small-offset-9'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.10', 'small-offset-10'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.11', 'small-offset-11'],
                [$ll . 'tt_content.tx_mffoundation_offsets.I.12', 'small-offset-12'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_offsetm' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_offsetm',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.1', 'medium-offset-1'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.2', 'medium-offset-2'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.3', 'medium-offset-3'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.4', 'medium-offset-4'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.5', 'medium-offset-5'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.6', 'medium-offset-6'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.7', 'medium-offset-7'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.8', 'medium-offset-8'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.9', 'medium-offset-9'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.10', 'medium-offset-10'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.11', 'medium-offset-11'],
                [$ll . 'tt_content.tx_mffoundation_offsetm.I.12', 'medium-offset-12'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_offsetl' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_offsetl',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.1', 'large-offset-1'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.2', 'large-offset-2'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.3', 'large-offset-3'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.4', 'large-offset-4'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.5', 'large-offset-5'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.6', 'large-offset-6'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.7', 'large-offset-7'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.8', 'large-offset-8'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.9', 'large-offset-9'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.10', 'large-offset-10'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.11', 'large-offset-11'],
                [$ll . 'tt_content.tx_mffoundation_offsetl.I.12', 'large-offset-12'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_collapses' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_collapses',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_collapses.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_collapses.I.1', 'small-collapse'],
                [$ll . 'tt_content.tx_mffoundation_collapses.I.2', 'small-uncollapse'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_collapsem' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_collapsem',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_collapsem.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_collapsem.I.1', 'medium-collapse'],
                [$ll . 'tt_content.tx_mffoundation_collapsem.I.2', 'medium-uncollapse'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_collapsel' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_collapsel',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_collapsel.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_collapsel.I.1', 'large-collapse'],
                [$ll . 'tt_content.tx_mffoundation_collapsel.I.2', 'large-uncollapse '],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_centereds' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_centereds',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_centereds.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_centereds.I.1', 'small-centered'],
                [$ll . 'tt_content.tx_mffoundation_centereds.I.2', 'small-uncentered'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_centeredm' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_centeredm',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_centeredm.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_centeredm.I.1', 'medium-centered'],
                [$ll . 'tt_content.tx_mffoundation_centeredm.I.2', 'medium-uncentered'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_centeredl' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_centeredl',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_centeredl.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_centeredl.I.1', 'large-centered'],
                [$ll . 'tt_content.tx_mffoundation_centeredl.I.2', 'large-uncentered'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_orderings' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_orderings',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_orderings.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.1', 'small-reset-order'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.2', 'small-push-1'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.3', 'small-push-2'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.4', 'small-push-3'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.5', 'small-push-4'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.6', 'small-push-5'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.7', 'small-push-6'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.8', 'small-push-7'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.9', 'small-push-8'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.10', 'small-push-9'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.11', 'small-push-10'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.12', 'small-push-11'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.13', 'small-push-12'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.14', 'small-pull-1'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.15', 'small-pull-2'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.16', 'small-pull-3'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.17', 'small-pull-4'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.18', 'small-pull-5'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.19', 'small-pull-6'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.20', 'small-pull-7'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.21', 'small-pull-8'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.22', 'small-pull-9'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.23', 'small-pull-10'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.24', 'small-pull-11'],
                [$ll . 'tt_content.tx_mffoundation_orderings.I.25', 'small-pull-12'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_orderingm' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_orderingm',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.1', 'medium-reset-order'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.2', 'medium-push-1'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.3', 'medium-push-2'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.4', 'medium-push-3'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.5', 'medium-push-4'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.6', 'medium-push-5'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.7', 'medium-push-6'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.8', 'medium-push-7'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.9', 'medium-push-8'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.10', 'medium-push-9'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.11', 'medium-push-10'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.12', 'medium-push-11'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.13', 'medium-push-12'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.14', 'medium-pull-1'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.15', 'medium-pull-2'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.16', 'medium-pull-3'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.17', 'medium-pull-4'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.18', 'medium-pull-5'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.19', 'medium-pull-6'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.20', 'medium-pull-7'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.21', 'medium-pull-8'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.22', 'medium-pull-9'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.23', 'medium-pull-10'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.24', 'medium-pull-11'],
                [$ll . 'tt_content.tx_mffoundation_orderingm.I.25', 'medium-pull-12'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_orderingl' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_orderingl',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.1', 'large-reset-order'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.2', 'large-push-1'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.3', 'large-push-2'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.4', 'large-push-3'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.5', 'large-push-4'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.6', 'large-push-5'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.7', 'large-push-6'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.8', 'large-push-7'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.9', 'large-push-8'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.10', 'large-push-9'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.11', 'large-push-10'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.12', 'large-push-11'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.13', 'large-push-12'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.14', 'large-pull-1'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.15', 'large-pull-2'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.16', 'large-pull-3'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.17', 'large-pull-4'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.18', 'large-pull-5'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.19', 'large-pull-6'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.20', 'large-pull-7'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.21', 'large-pull-8'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.22', 'large-pull-9'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.23', 'large-pull-10'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.24', 'large-pull-11'],
                [$ll . 'tt_content.tx_mffoundation_orderingl.I.25', 'large-pull-12'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],


    'tx_mffoundation_blockgrids' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_blockgrids',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.1', 'small-up-1'],
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.2', 'small-up-2'],
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.3', 'small-up-3'],
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.4', 'small-up-4'],
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.5', 'small-up-5'],
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.6', 'small-up-6'],
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.7', 'small-up-7'],
                [$ll . 'tt_content.tx_mffoundation_blockgrids.I.8', 'small-up-8'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_blockgridm' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_blockgridm',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.1', 'medium-up-1'],
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.2', 'medium-up-2'],
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.3', 'medium-up-3'],
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.4', 'medium-up-4'],
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.5', 'medium-up-5'],
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.6', 'medium-up-6'],
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.7', 'medium-up-7'],
                [$ll . 'tt_content.tx_mffoundation_blockgridm.I.8', 'medium-up-8'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_blockgridl' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_blockgridl',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.1', 'large-up-1'],
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.2', 'large-up-2'],
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.3', 'large-up-3'],
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.4', 'large-up-4'],
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.5', 'large-up-5'],
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.6', 'large-up-6'],
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.7', 'large-up-7'],
                [$ll . 'tt_content.tx_mffoundation_blockgridl.I.8', 'large-up-8'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],

    /* Begin: mfell additions / not Foundation */
    'tx_mffoundation_blockgridcollapse' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_blockgridcollapse',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_blockgridcollapse.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_blockgridcollapse.I.1', 'block-grid-collapse'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_vgaps' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_vgaps',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_vgaps.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_vgaps.I.1', 'small-vgap'],
                [$ll . 'tt_content.tx_mffoundation_vgaps.I.2', 'small-novgap'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_vgapm' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_vgapm',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_vgapm.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_vgapm.I.1', 'medium-vgap'],
                [$ll . 'tt_content.tx_mffoundation_vgapm.I.2', 'medium-novgap'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_vgapl' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_vgapl',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_vgapl.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_vgapl.I.1', 'large-vgap'],
                [$ll . 'tt_content.tx_mffoundation_vgapl.I.2', 'large-novgap'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_headerfontsize' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_headerfontsize',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_headerfontsize.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_headerfontsize.I.1', 'header-h1-font-size'],
                [$ll . 'tt_content.tx_mffoundation_headerfontsize.I.2', 'header-h2-font-size'],
                [$ll . 'tt_content.tx_mffoundation_headerfontsize.I.3', 'header-h3-font-size'],
                [$ll . 'tt_content.tx_mffoundation_headerfontsize.I.4', 'header-p-font-size'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_headercolor' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_headercolor',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                [$ll . 'tt_content.tx_mffoundation_headercolor.I.0', ''],
                [$ll . 'tt_content.tx_mffoundation_headercolor.I.1', 'header-color1'],
                [$ll . 'tt_content.tx_mffoundation_headercolor.I.2', 'header-color2'],
                [$ll . 'tt_content.tx_mffoundation_headercolor.I.3', 'header-color3'],
                [$ll . 'tt_content.tx_mffoundation_headercolor.I.4', 'header-color4'],
                [$ll . 'tt_content.tx_mffoundation_headercolor.I.5', 'header-color5'],
            ],
            'size' => 1,
            'maxitems' => 1,
        ],
    ],
    'tx_mffoundation_link' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_link',
        'config' => [
            'type' => 'input',
            'size' => '30',
            'wizards' => [
                '_PADDING' => 2,
                'link' => [
                    'type' => 'popup',
                    'title' => 'Link',
                    'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_link.gif',
                    'module' => [
                        'name' => 'wizard_link',
                        'urlParameters' => [
                            'mode' => 'wizard',
                            'act' => 'file'
                        ]
                    ],
                    'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1'
                ],
            ],
        ],
    ],
    'tx_mffoundation_overline' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_overline',
        'config' => [
            'type' => 'input',
            'size' => 50,
        ],
    ],
    'tx_mffoundation_subline' => [
        'exclude' => 1,
        'label' => $ll . 'tt_content.tx_mffoundation_subline',
        'config' => [
            'type' => 'input',
            'size' => 50,
        ],
    ],
    /* End: mfell additions / not Foundation */
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $foundationColumns);


// add special news palette
$mfFoundationPalettes = [
    'mffoundationGridColumnsPalette' => [
        'showitem' => 'tx_mffoundation_columnss,tx_mffoundation_columnsm,tx_mffoundation_columnsl',
    ],
    'mffoundationGridScreenSizePalette' => [
        'showitem' => 'tx_mffoundation_showbyscreensize, tx_mffoundation_hidebyscreensize',
    ],
    'mffoundationGridOffsetPalette' => [
        'showitem' => 'tx_mffoundation_offsets, tx_mffoundation_offsetm, tx_mffoundation_offsetl',
    ],
    'mffoundationGridCollapsesPalette' => [
        'showitem' => 'tx_mffoundation_collapses, tx_mffoundation_collapsem, tx_mffoundation_collapsel',
    ],
    'mffoundationGridCenteredPalette' => [
        'showitem' => 'tx_mffoundation_centereds, tx_mffoundation_centeredm, tx_mffoundation_centeredl',
    ],
    'mffoundationGridOrderingPalette' => [
        'showitem' => 'tx_mffoundation_orderings, tx_mffoundation_orderingm, tx_mffoundation_orderingl',
    ],
    'mffoundationGridIncompletePalette' => [
        'showitem' => 'tx_mffoundation_incompleterow',
    ],
    'mffoundationGridFluidRowPalette' => [
        'showitem' => 'tx_mffoundation_fluidrow',
    ],
    'mffoundationGridGapPalette' => [
        'showitem' => 'tx_mffoundation_vgaps, tx_mffoundation_vgapm, tx_mffoundation_vgapl',
    ],
    'mffoundationGridEqualizerPalette' => [
        'showitem' => 'tx_mffoundation_sameheight',
    ],
    'mffoundationBlockGridPalette' => [
        'showitem' => 'tx_mffoundation_blockgrids, tx_mffoundation_blockgridm, tx_mffoundation_blockgridl,
                        --linebreak--, tx_mffoundation_blockgridcollapse',
    ],

    'mffoundationOthersLinkPalette' => [
        'showitem' => 'tx_mffoundation_link',
    ],

    'mffoundationHeaderPalette' => [
        'showitem' => 'tx_mffoundation_headerfontsize, tx_mffoundation_headercolor',
    ],
    'mffoundationHeaderAddsPalette' => [
        'showitem' => 'tx_mffoundation_overline, --linebreak--, tx_mffoundation_subline',
    ],
];

// merge own palettes with tt_content palettes
$GLOBALS['TCA']['tt_content']['palettes'] = array_merge_recursive(
    $GLOBALS['TCA']['tt_content']['palettes'],
    $mfFoundationPalettes
);

// add tab Grid
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    '--div--;' . $llBe . 'tab.grid,'
        . '--palette--;' . $llBe . 'palette.columns;mffoundationGridColumnsPalette,'
        . '--palette--;' . $llBe . 'palette.showhide;mffoundationGridScreenSizePalette,'
        . '--palette--;' . $llBe . 'palette.offset;mffoundationGridOffsetPalette,'
        . '--palette--;' . $llBe . 'palette.incomplete;mffoundationGridIncompletePalette,'
        . '--palette--;' . $llBe . 'palette.fluidrow;mffoundationGridFluidRowPalette,'
        . '--palette--;' . $llBe . 'palette.centered;mffoundationGridCenteredPalette,'
        . '--palette--;' . $llBe . 'palette.ordering;mffoundationGridOrderingPalette,'
        . '--palette--;' . $llBe . 'palette.collapse;mffoundationGridCollapsesPalette,'
        . '--palette--;' . $llBe . 'palette.gap;mffoundationGridGapPalette,'
        . '--palette--;' . $llBe . 'palette.equalizer;mffoundationGridEqualizerPalette,'
        . '--palette--;' . $llBe . 'palette.blockgrid;mffoundationBlockGridPalette'
);

// add tab others
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    '--div--;' . $llBe . 'tab.others,--palette--;' . $llBe . 'palette.link;mffoundationOthersLinkPalette'
);

// add fields after header
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    '--palette--;' . $llBe . 'palette.headeradds;mffoundationHeaderAddsPalette',
    '',
    'after:header'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    '--palette--;' . $llBe . 'palette.header;mffoundationHeaderPalette',
    '',
    'after:header'
);
