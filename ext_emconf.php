<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'ZURB Foundation Support',
    'description' => 'ZURB Foundation Support, some more additions',
    'category' => 'fe',
    'author' => 'Marc Fell',
    'author_email' => 'info@marc-fell.de',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => true,
    'author_company' => '',
    'version' => '7.6.1',
    'modify_tables' => 'tt_content',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.2-7.6.99',
            'gridelements' => '3.1.0-99.99.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
