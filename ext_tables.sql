#
# Table structure for table 'tt_content'
#

#
# tx_mffoundation_blockgridcollapse -> weg?
#

CREATE TABLE tt_content (
	tx_mffoundation_columnss varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_columnsm varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_columnsl varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_showbyscreensize varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_hidebyscreensize varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_incompleterow varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_sameheight varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_offsets varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_offsetm varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_offsetl varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_collapses varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_collapsem varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_collapsel varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_centereds varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_centeredm varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_centeredl varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_orderings varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_orderingm varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_orderingl varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_blockgrids varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_blockgridm varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_blockgridl varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_blockgridcollapse varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_fluidrow varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_vgaps varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_vgapm varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_vgapl varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_headerfontsize varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_headercolor varchar(20) DEFAULT '' NOT NULL,
	tx_mffoundation_overline varchar(255) DEFAULT '',
	tx_mffoundation_subline varchar(255) DEFAULT '',
	tx_mffoundation_link tinytext
);